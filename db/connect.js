const mongoose = require('mongoose');


const connectDb = (url) =>{
    console.log('checking url', url)
    return mongoose.connect("mongodb://localhost/AlienDBex", {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: false
    })
}

module.exports = connectDb;